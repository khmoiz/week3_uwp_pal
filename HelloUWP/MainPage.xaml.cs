﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace HelloUWP
{
    /// <summary>
    /// The UI for this single-page application. Allows the user to press a button 
    /// to see the image
    /// </summary>
    public sealed partial class MainPage : Page
    {
        /// <summary>
        /// The constructor of the class
        /// </summary>
        public MainPage()
        {
            this.InitializeComponent();
        }

        /// <summary>
        /// Event handler that is called when the user clicks on _btnSayHello
        /// </summary>
        /// <param name="sender">The button _btnSayHello</param>
        /// <param name="e">not used</param>
        private void OnShowAvatar(object sender, RoutedEventArgs e)
        {
            //set the source of the image to the image asset 
            _imgHello.Source = new BitmapImage(new Uri("ms-appx:///Assets/HomerSimpson2.gif"));

            //change the caption of the button
            _btnHello.Content = "Tap the image to help Homer";
        }

        /// <summary>
        /// BONUS: Event handler called when the user taps on the image. If the image is an animated
        /// GIF the tap wil play or stop the animation
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnImageTapped(object sender, TappedRoutedEventArgs e)
        {
            //access the image source as a BitmapImage object which is what it was set to
            BitmapImage img = _imgHello.Source as BitmapImage;
            if (img.IsAnimatedBitmap)
            {
                //toggle the animation 
                if (img.IsPlaying)
                {
                    img.Stop();
                }
                else
                {
                    img.Play();
                }
            }

        }
    }
}
